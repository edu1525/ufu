import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.time.Instant;
import java.util.ArrayList;

public class WorkerRunnable implements Runnable{
	Socket socket;
	Arvore arvore;
	
	public WorkerRunnable(Socket clientSocket, Arvore arvore) {
        this.socket = clientSocket;
        this.arvore = arvore;
    }

	@Override
	public void run() {
		
		// READ AN HTTP REQUEST FROM CLIENT
		InputStreamReader input;
		
		try {
			input = new InputStreamReader(socket.getInputStream());
			
			int contentLenght = -1;			
			
			BufferedReader buffer = new BufferedReader(input);
			
			ArrayList<String> lines = new ArrayList<>();
			
			String line = null;
	
			if(buffer != null) line = buffer.readLine();
			
			
			while(line != null && !line.isEmpty()){
				
				String[] split = line.split(" ");
				
				if(split[0].equals("Content-Length:")) contentLenght = Integer.parseInt(split[1]);
				
				lines.add(line);					
		
					line = buffer.readLine();
				
				
			}
			
				
			
			line = lines.get(0); // line guarda a primeira linha da requisição
			
			if(line != null && !line.isEmpty()){			
				String[] partes = line.split(" ");					
				
				System.out.println(line);
				
				// GET ---------------------------------------------------------------------------------
				
				if(partes[0].trim().equals("GET")){ // READ THE HTTP VERB
				
					// PREPARE AN HTTP RESPONSE
					// SEND HTTP RESPONSE TO THE CLIENT
					String response;
					
					No busca;
					if((busca = arvore.buscaNo(partes[1])) != null){
						if(busca.getDados().isEmpty()) response = "HTTP/1.1 204 No Content\n";
						else response = "HTTP/1.1 200 OK\n";					
						
						response += "Version: "+busca.getVersao()+"\n"+"Creation: "+
					busca.getHoraCriacao()+"\n"+"Modification: "+busca.getHoraModificacao()+"\n\n";
						
						if(!busca.getDados().isEmpty()) {
							
							for(String s : busca.getDados()){
								response += s+"\n";
						}
					}			
						
					} else{ // PAGE NOT FOUND
						response = "HTTP/1.1 404 PAGE NOT FOUND\n";							
					}					
						
					try {
						socket.getOutputStream().write(response.getBytes("UTF-8"));
						socket.close();
						
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
						
				}
				
				// -----------------------------------------------------------------------------------
				
				// POST ------------------------------------------------------------------------------
				if(partes[0].trim().equals("POST")){
					
					StringBuilder builder = new StringBuilder();
					
					for(int i = 0; i < contentLenght; i++){
						try {
							builder.append((char) buffer.read());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}						
					line = builder.toString();
					if(line != null) lines.add(line);
					
					System.out.println(lines.get(lines.size()-1));
					No post, no;
					
					String response;
					
					if((no = arvore.buscaNo(partes[1])) != null && ServidorHTTP.nosEmUso.contains(no))
						response = "HTTP/1.1 423 Locked\n";
						
					else if((post = arvore.criaArquivo(partes[1], lines.get(lines.size()-1))) != null){
						
						response = "HTTP/1.1 200 OK\n"+"Version: "+post.getVersao()+"\nCreation: "
					+post.getHoraCriacao()+"\nModification: "+post.getHoraModificacao()+"\n";	
						
					} else{
						post = arvore.buscaNo(partes[1]);
						
						response = "HTTP/1.1 409 Conflict\n"+"Version: "+post.getVersao()+"\nCreation: "
								+post.getHoraCriacao()+"\nModification: "+post.getHoraModificacao()+"\n";
					}
							
					
					
					try {
						socket.getOutputStream().write(response.getBytes("UTF-8"));
						socket.close();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				}
				
				// ----------------------------------------------------------------------------------
				
				// PUT ------------------------------------------------------------------------------
				if(partes[0].trim().equals("PUT")){
					
					StringBuilder builder = new StringBuilder();
					
					for(int i = 0; i < contentLenght; i++){
						try {
							builder.append((char) buffer.read());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
					
					line = builder.toString();
					if(line != null) lines.add(line);
					
					No put;
					String response;
					
					if((put = arvore.buscaNo(partes[1])) != null && !ServidorHTTP.nosEmUso.contains(put)){
						put.inserirDado(lines.get(lines.size()-1));
						put.setHoraModificacao(String.valueOf((Instant.now().toEpochMilli())));
						put.setVersao(put.getVersao()+1);
						
						response = "HTTP/1.1 200 OK\n"+"Version: "+put.getVersao()+"\nCreation: "
								+put.getHoraCriacao()+"\nModification: "+put.getHoraModificacao()+"\n\n";	
									
					} else if(ServidorHTTP.nosEmUso.contains(put)) response = "HTTP/1.1 423 Locked\n";
					
					else response = "HTTP/1.1 404 PAGE NOT FOUND\n\n";
													
					try {
						socket.getOutputStream().write(response.getBytes("UTF-8"));
						socket.close();
						
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}			
						
					
				
				}
				// ---------------------------------------------------------------------------------------
				// HEAD ----------------------------------------------------------------------------------
				if(partes[0].trim().equals("HEAD")){
					
					No head;
					String response;
					
					if((head = arvore.buscaNo(partes[1])) != null){
						response = "HTTP/1.1 200 OK\n"+"Version: "+head.getVersao()+"\nCreation: "
								+head.getHoraCriacao()+"\nModification: "+head.getHoraModificacao();	
					
					} else response = "HTTP/1.1 404 PAGE NOT FOUND\n";
					
					try {
						socket.getOutputStream().write(response.getBytes("UTF-8"));
						socket.close();
						
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
				
				}
				// ---------------------------------------------------------------------------------------
				
				// DELETE --------------------------------------------------------------------------------
				if(partes[0].trim().equals("DELETE")){
					
					String response;
					
					if(arvore.removeNo(partes[1])) response = "HTTP/1.1 200 OK\n";
					else response = "HTTP/1.1 404 PAGE NOT FOUND\n";
					
					
					try {
						socket.getOutputStream().write(response.getBytes("UTF-8"));
						socket.close();
						
					} catch (UnsupportedEncodingException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				}
				
				// ---------------------------------------------------------------------------------------
			}
	
			
		}
	

			
		 catch (IOException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
	

	}
		
		
		
		
		
	

}
