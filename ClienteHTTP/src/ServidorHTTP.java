import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.time.Instant;
import java.util.ArrayList;


public class ServidorHTTP {
	public static Arvore arvore;
	Socket socket;
	ServerSocket server;
	
	// GUARDA OS N�S QUE EST�O SENDO UTILIZADOS PARA TRATAR CONCORR�NCIA	
	public static ArrayList<No> nosEmUso = new ArrayList<No>(); 
	
	
	public static void main(String[] args) throws IOException  {
		int port;		
		port = Integer.parseInt(args[0]);	
		
		No raiz = new No();
		raiz.setNome("");
		
		raiz.inserirDado("Eduardo de Oliveira Freitas");
		raiz.inserirDado("Cientista da Computa��o");
		raiz.setHoraModificacao(String.valueOf((Instant.now().toEpochMilli())));
		raiz.setVersao(raiz.getVersao()+1);
		
		No filho1 = new No();		
		No filho2 = new No();
		
		filho1.setNome("pessoa");
		filho1.inserirDado("Rua Hedd Lamar Ribeiro, 2090");
		
		filho1.setHoraModificacao(String.valueOf((Instant.now().toEpochMilli())));
		
		filho2.setNome("idade");
		
		filho2.setHoraModificacao(String.valueOf((Instant.now().toEpochMilli())));
		
		raiz.inserirFilho(filho1);
		filho1.inserirFilho(filho2);
				
		arvore = new Arvore(raiz);
		
		new ServidorHTTP().startServer(port);
				
	}
	
	public void startServer(int port) throws IOException{
		try {
			server = new ServerSocket(port);
		} catch (IOException e2) {
			e2.printStackTrace();
		}		
		
		server.setReuseAddress(true);
		
		System.out.println("Aguardando por conex�es na porta "+port);
		
		while(true){			
			Socket socket = null;
			
			try{
				socket = server.accept();
			}catch(IOException e){
				e.printStackTrace();
			}
			
			new Thread(
	                new WorkerRunnable(socket, arvore)
	            ).start();
			

		}
			
	}
}

