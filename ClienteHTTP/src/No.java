import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class No {
	private String nome;
	private List<No> filhos = new ArrayList<No>();
	private List<String> dados = new ArrayList<String>();
	private String horaCriacao = String.valueOf((Instant.now().toEpochMilli()));
	private String horaModificacao;
	private int versao = 0;
	
	public boolean inserirDado(String dado){
		return dados.add(dado);
	}
	
	public boolean inserirFilho(No filho){
		return filhos.add(filho);
		
	}	
	
	public boolean removerDado(String dado){
		return dados.remove(dado);
		
	}
	
	public boolean removerFilho(No filho){
		return filhos.remove(filho);
	}
	
	// GETTERS E SETTERS ----------------------
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public List<No> getFilhos() {
		return filhos;
	}
	public void setFilhos(List<No> filhos) {
		this.filhos = filhos;
	}
	public List<String> getDados() {
		return dados;
	}
	public void setDados(List<String> dados) {
		this.dados = dados;
	}
	public String getHoraCriacao() {
		return horaCriacao;
	}
	public void setHoraCriacao(String horaCriacao) {
		this.horaCriacao = horaCriacao;
	}
	public String getHoraModificacao() {
		return horaModificacao;
	}
	public void setHoraModificacao(String horaModificacao) {
		this.horaModificacao = horaModificacao;
	}
	public int getVersao() {
		return versao;
	}
	public void setVersao(int versao) {
		this.versao = versao;
	}
	
	public boolean equals(Object obj){
		No no = (No) obj;
		if(no != null && no.getNome().equals(this.nome)) return true;
		else return false;
	}
}
