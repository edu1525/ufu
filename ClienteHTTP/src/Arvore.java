import java.time.Instant;

public class Arvore {
	private No raiz;
	
	public Arvore(No raiz){
		this.raiz = raiz;		
	}
	
	public No buscaNo(String caminho){
		
		// BLOQUEIA O N� PARA TRATAR CONCORR�NCIA
		No bloqueado = new No();
		String[] nome = caminho.split("/");
		bloqueado.setNome(nome[nome.length-1]);
		ServidorHTTP.nosEmUso.add(bloqueado);
		// --------------------------------------
				
		if(caminho.charAt(0) == '/'){
			// REMOVE A PRIMEIRA / CASO ELA EXISTA
			StringBuilder sb = new StringBuilder(caminho);
			sb.deleteCharAt(0);
			caminho = sb.toString();			
		}
		
		if(caminho.isEmpty()) return raiz;
		
		String[] pastas = caminho.split("/");
		
		No busca = null, atual = raiz;
		
		for(String pasta : pastas){
			No aux = new No();
			aux.setNome(pasta);
			
			int index = atual.getFilhos().indexOf(aux);			
			if(index != -1) busca = atual.getFilhos().get(index);
			
			else{
				ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
				return null;
			}
			
			atual = busca;
		}
		
		ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
		return busca;		
	}
	
	public No criaArquivo(String caminho, String dados){
		
		// BLOQUEIA O N� PARA TRATAR CONCORR�NCIA
		No bloqueado = new No();
		String[] nome = caminho.split("/");
		bloqueado.setNome(nome[nome.length-1]);
		ServidorHTTP.nosEmUso.add(bloqueado);
		// --------------------------------------
		
		if(caminho.charAt(0) == '/'){
			// REMOVE A PRIMEIRA / CASO ELA EXISTA
			StringBuilder sb = new StringBuilder(caminho);
			sb.deleteCharAt(0);
			caminho = sb.toString();			
		}
		
		ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
		if(caminho.isEmpty()) return null;
		
		
		
		String[] pastas = caminho.split("/");
		
		if(pastas.length == 1){ // Criar arquivo na raiz do sistema
			if(buscaNo(pastas[0]) == null){ // Esse arquivo ainda n�o existe, cri�-lo
				No arquivo = new No();
				
				arquivo.setNome(pastas[0]);
				arquivo.inserirDado(dados);
				arquivo.setHoraCriacao(String.valueOf(Instant.now().toEpochMilli()));
				arquivo.setHoraModificacao(String.valueOf(Instant.now().toEpochMilli()));
				
				raiz.inserirFilho(arquivo);
				
				ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
				return arquivo;
			}
			
		}else{ // Criar arquivo em subpastas da ra�z
			No busca = null, aux;
			int i = 0;
			String caminhoAtual="";
			
			while(i < pastas.length - 1){	
				caminhoAtual += pastas[i]+"/";
				if((busca = buscaNo(caminhoAtual)) == null){ // Se n�o existe o n�
					
					ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
					return null;
				}
				i++;				
			}
			
			caminhoAtual += pastas[i];
			if((aux = buscaNo(caminhoAtual)) == null){ // Se n�o existe o n�
				No arquivo = new No();
				
				arquivo.setNome(pastas[i]);
				arquivo.inserirDado(dados);
				arquivo.setHoraCriacao(String.valueOf(Instant.now().toEpochMilli()));
				arquivo.setHoraModificacao(String.valueOf(Instant.now().toEpochMilli()));
				
				busca.inserirFilho(arquivo);
				
				ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
				return arquivo;
				
			} else{
				ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
				return null;
			}
		}		
		
		ServidorHTTP.nosEmUso.remove(bloqueado); // Libera o n� 
		return null;
	}
	
	public boolean removeNo(String caminho){
		if(caminho.charAt(0) == '/'){
			// REMOVE A PRIMEIRA / CASO ELA EXISTA
			StringBuilder sb = new StringBuilder(caminho);
			sb.deleteCharAt(0);
			caminho = sb.toString();			
		}
		
		if(caminho.isEmpty()) return false;
		
		String[] pastas = caminho.split("/");
		
		if(pastas.length == 1){ // remover arquivo na raiz do sistema
			if(buscaNo(pastas[0]) == null){ // Esse arquivo n�o existe
								
				return false;
			}else{
				No auxx = new No();
				auxx.setNome(pastas[0]);
				
				raiz.removerFilho(auxx);
				return true;
			}
			
		}else{ // remover arquivo em subpastas da ra�z
			No busca = null, aux;
			int i = 0;
			String caminhoAtual="";
			
			while(i < (pastas.length - 1)){	
				caminhoAtual += pastas[i]+"/";
				if((busca = buscaNo(caminhoAtual)) == null){ // Se n�o existe o n�
					return false;
				}
				i++;				
			}
			caminhoAtual += pastas[i];
			if((aux = buscaNo(caminhoAtual)) == null){ // Se n�o existe o n�
				return false;
				
			} else{
				No auxx = new No();
				auxx.setNome(aux.getNome());
				
				busca.removerFilho(auxx);
				return true;
			}
		}		
	}
	
	public String changeCharInPosition(int position, char ch, String str){
	    char[] charArray = str.toCharArray();
	    charArray[position] = ch;
	    return new String(charArray);
	}
	

	public No getRaiz() {
		return raiz;
	}

	public void setRaiz(No raiz) {
		this.raiz = raiz;
	}
}
